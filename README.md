Python Projects
---

A collection of Python scripts created by me to do useful and fun things.

[**Pi Clock** to tell the time](https://github.com/JoshMarsden/python-projects/tree/master/pi-clock)
